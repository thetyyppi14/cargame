﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarGame
{
    public class CarGame : Game
    {
        SpriteBatch SpriteBatch { get; set; }

        Texture2D Spritesheet { get; set; }

        SpriteFont Font { get; set; }

        Train train = new Train();

        List<Car> Cars = new List<Car>
        {
            new Car(250, Keys.W, Keys.S, Keys.A, Keys.D, Keys.Space, 180, 235),
            new Car(350, Keys.Up, Keys.Down, Keys.Left, Keys.Right, Keys.Enter, 382, 405)
        };

        double laptime = 0;
        bool timerrunning = false;
        bool gameover = false;

        public CarGame()
        {
           var manager = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferHeight = 1000,
                PreferredBackBufferWidth = 1000
            };

        }

        protected override void Initialize()
        {

            using (var stream = TitleContainer.OpenStream("spritesheet.png"))
            {
                Spritesheet = Texture2D.FromStream(GraphicsDevice, stream);
            }

            Font = Content.Load<SpriteFont>("Font24");

            this.IsMouseVisible = true;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.LightGreen);
            SpriteBatch.Begin();

            DrawRectangle(100, 100, 800, 800, Color.Gray);
            DrawRectangle(300, 300, 400, 400, Color.Wheat);

            foreach (var car in Cars)
            {
                DrawSprite((int)car.x, (int)car.y, car.Imagex, car.Imagey, 120, 390, (float)car.rotation, 0.2f);
            }

            DrawSprite((int)train.x, (int)train.y, 10, 60, 130, 800, 0, 0.2f);
            if (gameover)
            {
                DrawString(450, 490, "Game Over", 1);
            }

            SpriteBatch.End();
            base.Draw(gameTime);
        }

        protected override void Update(GameTime gameTime)
        {
            var mouse = Mouse.GetState();
            var keyboard = Keyboard.GetState();

            if (gameover && mouse.LeftButton == ButtonState.Pressed)
            {
                foreach (var car in Cars)
                {
                    gameover = false;
                    car.x = car.initialx;
                    car.y = 250;
                    car.rotation = 0;
                    car.speed = 0;
                }
            }

            if (gameover)
            {
                return;
            }

            foreach (var car in Cars)
            {
                if (car.x > 1000 || car.y > 1000 || car.x < 0 || car.y < 0)
                {
                    gameover = true;
                    continue;
                }
                if (car.x > 300 && car.x < 700 && car.y > 300 && car.y < 700)
                {
                    gameover = true;
                    continue;
                }
                if (train.Collides((int)car.x, (int)car.y))
                {
                    gameover = true;
                    continue;
                }
                if (car.y > 475 && car.y < 500 && car.x < 300)
                {

                    if (timerrunning)
                    {
                        timerrunning = false;
                        Console.WriteLine($"lap time: {laptime}");
                        laptime = 0;
                    }
                }
                if (car.y > 500 && car.y < 525 && car.x < 300)
                {
                    if (!timerrunning)
                    {
                        timerrunning = true;
                    }
                }
                if (timerrunning)
                {
                    laptime = laptime + gameTime.ElapsedGameTime.TotalMilliseconds;
                }

                car.Update(keyboard);
            }

            train.x = train.x - train.speed;

            if (train.x < -2000)
            {
                train.x = 2000;
            }
        }

        void DrawSprite(int drawX, int drawY, int imageX, int imageY, int imageHeight, int imageWidth, float rotation, float scale)
        {
            SpriteBatch.Draw(Spritesheet, new Vector2(drawX - (int)Cars[0].x + 500, drawY - (int)Cars[0].y + 500), new Rectangle(imageX, imageY, imageWidth, imageHeight), Color.White, rotation, new Vector2(imageWidth / 2, imageHeight / 2), scale, SpriteEffects.None, 1);
        }

        void DrawRectangle(int drawX, int drawY, int width, int height, Color color)
        {
            Texture2D texture = new Texture2D(GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            texture.SetData(new[] { Color.White });
            Rectangle area = new Rectangle(drawX - (int)Cars[0].x + 500, drawY - (int)Cars[0].y + 500, width, height);
            SpriteBatch.Draw(texture, area, color);
        }

        void DrawString(int x, int y, string text, float scale)
        {
            SpriteBatch.DrawString(Font, text, new Vector2(x, y), Color.Black, 0, new Vector2(0, 0), scale, SpriteEffects.None, 1);
        }
    }
}
