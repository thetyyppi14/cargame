﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarGame
{
    class Train
    {
        public double x = 500;
        public double y = 500;
        public double speed = 4;

        public bool Collides(int carX, int carY)
        {
            var trainFront = x - 80;
            var trainBack = x + 80;
            var trainUp = y - 13;
            var trainDown = y + 13;

            if (carX > trainFront && carX < trainBack && carY > trainUp && carY < trainDown)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
