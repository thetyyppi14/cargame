﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarGame
{
    public class Car
    {
        public double x;
        public double y = 250;
        public double rotation = 0;
        public double speed = 0;
        public double initialx;
        public int Imagex;
        public int Imagey;
            

        Keys Forward;
        Keys Backwards;
        Keys Left;
        Keys Right;
        Keys Brake;

        public Car(int _x, Keys forward, Keys back, Keys left, Keys right, Keys brake, int imagex, int imagey)
        {
            x = _x;
            initialx = _x;
            Forward = forward;
            Backwards = back;
            Left = left;
            Right = right;
            Brake = brake;
            Imagex = imagex;
            Imagey = imagey;

        }

        public void Update(KeyboardState keyboard)
        {
            if (keyboard.IsKeyDown(Forward))
            {
                speed = speed + 0.1;
            }
            if (keyboard.IsKeyDown(Backwards))
            {
                speed = speed - 0.1;

            }
            if (keyboard.IsKeyDown(Right))
            {
                rotation = rotation + 0.01 * speed;
            }
            if (keyboard.IsKeyDown(Left))
            {
                rotation = rotation - 0.01 * speed;
            }
            if (keyboard.IsKeyDown(Brake))
            {
                if (speed > 0)
                {
                    speed = speed - 0.05;
                }
                if (speed < 0)
                {
                    speed = speed + 0.05;
                }
                if (speed < 0.05 && speed > -0.05)
                {
                    speed = 0;
                }
            }
            if (speed > 3)
            {
                speed = 3;
            }
            if (speed < -1)
            {
                speed = -1;
            }

            x = x - Math.Cos(rotation) * speed;
            y = y - Math.Sin(rotation) * speed;
        }
    }
}
